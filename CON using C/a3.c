/*
 * Regula Falsi or False Position method program
 * 
 * This is somewhat similar to Bisection and is faster than the former method.
 * But not all the time as this method may never converge to 0 at all!
 */

#include <stdio.h>
#include <math.h>

#define FUNCTION(x) pow(x, 2) - x - 2
#define ESP 0.0001

int main()
{
	double x1 = 1, x2 = 3, x3 = 0;
	double f1 = 0, f2 = 0, f3 = 0;
	unsigned i = 1;

	puts("Iterator\tx3\t\tf3");
	do {
		f1 = FUNCTION(x1);
		f2 = FUNCTION(x2);
		x3 = x2 - f2 * (x2-x1) / (f2-f1);
		f3 = FUNCTION(x3);
		printf("%i\t\t%f\t%f\n", i++, x3, f3);
		if(f3*f1 > 0)
			// f3 and f1 have equal sign
			x1 = x3;
		else
			// f3 and f1 have opposite sign
			x2 = x3;
	} while(fabs(f3) >= ESP);

	printf("Approximate root = %f\n", x3);

	return(0);
}
