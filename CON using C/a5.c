#include <stdio.h>
#include <math.h>

#define FUNCTION(x) pow(x, 2) - 4*x - 10
#define ESP 0.0001

int main()
{
	double x1 = 4, x2 = 2, x3 = 0;
	double f1 = 0, f2 = 0, f3 = 0;
	int i = 1;

	puts("Iterator\tx\t\tf");
	do {
		f1 = FUNCTION(x1);
		f2 = FUNCTION(x2);
		x3 = x2 - f2 * (x2-x1)/(f2-f1);
		f3 = FUNCTION(x3);
		printf("%d\t\t%f\t%f\n", i++, x3, f3);
		x1 = x2;
		x2 = x3;
	} while(fabs(f3) >= ESP);	// comparison???

       	return(0);
}
