#include <stdio.h>
#include <math.h>

#define FUNCTION(x) pow(x, 2) - 3*x + 2
#define FUNCTION_DX(x) 2*x - 3
#define ESP 0.000001
#define MAX_ITERATION 10

int main()
{
	float x = 0, x_kth;
	float f, fdx;
	float condition;
	int i = 1;

	puts("Iterator\tf\t\tfdx\t\tx kth");
	do {
		f = FUNCTION(x);
		fdx = FUNCTION_DX(x);
		x_kth = x - f/fdx;
		printf("%3i\t\t%f\t%f\t%f\n", i++, f, fdx, x_kth);
		condition = x_kth - x;	// don't understand this part :(
					// not what was inferred on wikipedia
		x = x_kth;
	} while(fabs(condition) > ESP);
	//} while(i <= MAX_ITERATION);
	
	printf("Approximate root = %f\n", x);

	return(0);
}
