/*
 * Program to convert binary to decimal number
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main()
{
	int binary_no = 0;

	printf("Enter the binary number: ");
	scanf("%d", &binary_no);

	int power = 0;
	int last_bit = 0;
	int decimal = 0;
	while(binary_no != 0) {
		last_bit = binary_no % 10;
		if(last_bit != 0 && last_bit != 1) {
			puts("Invalid binary number!");
			exit(1);
		}
		decimal = decimal + last_bit * pow(2, power);
		power = power + 1;
		binary_no = binary_no / 10;
	}

	printf("The decimal value is: %d\n", decimal);

	return(0);
}
