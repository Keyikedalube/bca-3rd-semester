/*
 * Write a program to find the root of a quadratic equation using Bisection Method
 *
 * Static program
 * Following what sir Priyo taught in the class
 */

#include <stdio.h>
#include <math.h>

#define FUNCTION(x) pow(x, 2) - 4*x - 10
#define ESP 0.0001

int main()
{
	double x2 = -2, x1 = -1, x3;
	double f1, f2, f3;
	unsigned i = 1;

	puts("Iterator\tx1\t\tx2\t\tx3\t\t\tf1\t\tf2\t\tf3");
	do {
		f1 = FUNCTION(x1);
		f2 = FUNCTION(x2);
		x3 = (x2 + x1) / 2;
		f3 = FUNCTION(x3);
		printf("%3i\t\t%9f\t%9f\t%9f\t\t%9f\t%9f\t%9f\n", i++, x1, x2, x3, f1, f2, f3);
		//
		// if(f3 > 0 && f1 > 0) doesn't yield proper output
		// don't know why is that...
		// Oren method of multiplying and comparing it with +ve/-ve is better
		//
		// still yet! Don't understand why the statement below does not work either
		// if(f3 > ESP && f2 > ESP)
		//
		if(f3*f1 > 0)
			// f3 and f1 have equal sign
			x1 = x3;
		else
			// f3 and f1 have opposite sign
			x2 = x3;
	} while(fabs(f3) >= ESP);

	printf("Approximate root = %9f\n", x3);

	return(0);
}
