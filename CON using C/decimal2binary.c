/*
 * Program to convert decimal to binary
 */

#include <stdio.h>

int main()
{
	printf("Enter the decimal number: ");
	int decimal = 0;
	scanf("%d", &decimal);

	char binary[33];	// as in 32 bit + null character
	int index = 0;
	short remainder = 0;
	// do-while is recommended as it enable to convert the
	// decimal value 0
	do {
		remainder = decimal % 2;
		if(remainder == 1)
			binary[index++] = '1';
		else
			binary[index++] = '0';
		decimal /= 2;
	} while(decimal != 0);

	printf("The binary equivalent is: ");
	// back one step from null
	index--;
	while(index >= 0)
		printf("%c", binary[index--]);
	puts("");

	return(0);
}
