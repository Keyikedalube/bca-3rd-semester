/*
 * Write a program for static class member
 */

#include <iostream>

using namespace std;

class Sentinel {
    static int value;

public:
    void set_value(int);
    void display_value();
};

void Sentinel::set_value(int temporary)
{
    value = temporary;
}

void Sentinel::display_value()
{
    cout << "The value now is: " << value;
    cout << endl;
}

// defining static variable
int Sentinel::value;

int main()
{
    Sentinel obj1, obj2;

    obj1.display_value();
    obj1.set_value(10);
    obj2.display_value();
    obj2.set_value(20);
    obj1.display_value();

    return 0;
}
