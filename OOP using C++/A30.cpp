/*
 * Write a program implementing pointers to objects
 */

#include <iostream>

using namespace std;

class Student {
    char name[30];
    int roll;

	public:
    void get(unsigned short index)
    {
        cout << "\tStudent " << index << " data\n";
        cout << "Enter name: ";
        cin >> name;
        cout << "Enter roll: ";
        cin >> roll;
    }
    void display(unsigned short index)
    {
        cout << "\tStudent " << index << " data\n";
        cout << "Name: " << name << endl;
        cout << "Roll: " << roll << endl;
    }
};

int main()
{
	Student *s1 = new Student();
    s1->get(1);

    Student s2, *s3 = &s2;
    s3->get(2);

    cout << endl << endl;
    s1->display(1);
    s3->display(2);
    
    // free s1 object from the heap memory, since it's allocated to it
    // using new operator
    delete s1;

    return 0;
}
