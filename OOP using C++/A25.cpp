/*
 * Write a program demonstrating multiple inheritance
 */

#include <iostream>

using namespace std;

class Base1 {
	protected:
	int x;
	
	public:
	void set_x(int x)
	{
		this->x = x;
	}

	int get_x()
	{
		return x;
	}
};

class Base2 {
	protected:
	int y;
	
	public:
	void set_y(int y)
	{
		this->y = y;
	}
	
	int get_y()
	{
		return y;
	}
};

class Child : private Base1, private Base2 {
	int sum;
	
	public:
	void set_x_y()
	{
		int temporary;
		cout << "Enter integer value for x: ";
		cin >> temporary;
		set_x(temporary);
		cout << "Enter integer value for y: ";
		cin >> temporary;
		set_y(temporary);
	}
		
	void compute_sum()
	{
		sum = get_x() + get_y();
		cout << "The sum of " << get_x() << " and " << get_y() << " is: ";
		cout << sum;
		cout << "\n";
	}
};

int main()
{
	Child object;
	
	object.set_x_y();
	object.compute_sum();
	
	return 0;
}
