/*
 * Write a program to illustrate the use of this pointer.
 */

#include <iostream>
#include <cstdlib>

using namespace std;

class Sentinel {
    int x;

public:
    void set_x(int);
    void display_x();
};

void Sentinel::set_x(int x)
{
    this->x = x;
}

void Sentinel::display_x()
{
    cout << "x value is " << x << endl;
}

int main()
{
    Sentinel obj;

    srandom(time(0));
    obj.set_x(random() % 10);
    obj.display_x();

    return 0;
}
