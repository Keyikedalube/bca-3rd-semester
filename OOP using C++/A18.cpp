/*
 * Write a program to demonstrate copy constructors.
 */

#include <iostream>

using namespace std;

class Sentinel {
	int datum;
	
	public:
	Sentinel() {}
	Sentinel(Sentinel &obj)
	{
		datum = obj.datum;
	}
	void get();
	void print();
};

void Sentinel::get()
{
	cout << "Enter integer data: ";
	cin >> datum;
}

void Sentinel::print()
{
	cout << datum << "\n";
}

int main()
{
	Sentinel obj1;
	obj1.get();
	
	// copy obj1 value
	Sentinel obj2(obj1);
	
	cout << "First object: ";
	obj1.print();
	cout << "Second object: ";
	obj2.print();
	
	return 0;
}
