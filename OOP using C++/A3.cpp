/*
 * Write a cpp program which explains the use of reference variable
 */

#include <iostream>

using namespace std;

int main()
{
	int a = 0;
	int &b = a;

	cout << "Enter some integer value: ";
	cin >> a;

	cout << "The value you entered is: " << b << endl;

	return 0;
}
