/*
 * Write a program which explain concept of an "array object"
 */

#include <iostream>

using namespace std;

class Array {
public :
    void display_element(int);
};

void Array::display_element(int iterator)
{
	cout << "Hello from element " << iterator << "\n";
}

int main()
{
	const int SIZE = 10;
	Array array_object[SIZE];

	for (int iterator = 0; iterator < SIZE; iterator++)
		array_object[iterator].display_element(iterator);

	return 0;
}
