/*
 * Write a cpp program for overloaded constructors.
 */

#include <iostream>

using namespace std;

class Sentinel {
    int x, y, z;

public:
    Sentinel(int);
    Sentinel(int, int);
    Sentinel(int, int, int);
    void display();
};

Sentinel::Sentinel(int x)
{
    this->x = x;
    y = 0;
    z = 0;
}

Sentinel::Sentinel(int x, int y)
{
    this->x = x;
    this->y = y;
    z = 0;
}

Sentinel::Sentinel(int x, int y, int z)
{
    this->x = x;
    this->y = y;
    this->z = z;
}

void Sentinel::display()
{
    cout << "The values of x, y, and z are: ";
    cout << x << ", " << y << ", and " << z << endl;
}

int main()
{
    Sentinel obj1(10);
    Sentinel obj2(10, 20);
    Sentinel obj3(10, 20, 30);

    obj1.display();
    obj2.display();
    obj3.display();

    return 0;
}
