/*
 * Write a program for arrays within a class
 */

#include <iostream>

using namespace std;

class Sentinel {
    static const int SIZE = 10;
    int values[SIZE];

public:
    void get_values();
    void display_values();
};

void Sentinel::get_values()
{
    cout << "Enter ten integer values:" << endl;
    for (int i = 0; i < SIZE; i++) {
        cin >> values[i];
    }
}

void Sentinel::display_values()
{
    cout << "The values you entered were:\n";
    for (int i = 0; i < SIZE; i++) {
        cout << values[i] << " ";
    }
    cout << endl;
}

int main()
{
    Sentinel obj;
    obj.get_values();
    obj.display_values();

    return 0;
}
