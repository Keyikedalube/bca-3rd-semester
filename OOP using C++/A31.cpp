/*
 * Write a program implementing array of pointers to objects that can be
 * used to access the individual objects.
 */

#include <iostream>

using namespace std;

class Student {
	char name[30];
    int roll;

	public:
    void get(unsigned short index)
    {
        cout << "\tStudent " << index << " data\n";
        cout << "Enter name: ";
        cin >> name;
        cout << "Enter roll: ";
        cin >> roll;
    }
    void display(unsigned short index)
    {
        cout << "\tStudent " << index << " data\n";
        cout << "Name: " << name << endl;
        cout << "Roll: " << roll << endl;
    }
};

int main()
{
	Student *list = new Student[3];
	
	for (unsigned short index = 0; index < 3; index++, list++)
		list->get(index);

	// return pointer to the first object
	list -= 3;
	
	cout << "\n";
	for (unsigned short index = 0; index < 3; index++, list++)
		list->display(index);
		
	// free the objects from the heap memory after returning the pointer
	// to the first object.
	list -= 3;
	delete []list;
		
	return 0;
}
