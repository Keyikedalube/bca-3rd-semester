/*
 * Write a CPP program that contains a class called 'temp' and member functions
 * for accepting a temperature in Fahrenheit and displays in Celsius.
 */

#include <iostream>

using namespace std;

class temp {
    double fahrenheit;
    double celsius;

public:
    void get_fahrenheit();
    void convert_to_Celsius();
    void display_Celsius();
};

void temp::get_fahrenheit()
{
    cout << "Enter temperature (Fahrenheit): ";
    cin >> fahrenheit;
}

void temp::convert_to_Celsius()
{
    celsius = (fahrenheit - 32) * 5/9;
}

void temp::display_Celsius()
{
    cout << "Temperature in Celsius: " << celsius;
    cout << "\n";
}

int main()
{
    temp temperature;

    temperature.get_fahrenheit();
    temperature.convert_to_Celsius();
    temperature.display_Celsius();

    return 0;
}
