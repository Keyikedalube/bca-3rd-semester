/*
 * Write a program for a friend function to two classes
 */

#include <iostream>

using namespace std;

// class declaration
class B;

class A
{
    int data;
    friend void add(A, B);

public:
    A()
    {
        data = 10;
    }
};

class B
{
    int data;
    friend void add(A, B);

public:
    B()
    {
        data = 20;
    }
};

void add(A obj_a, B obj_b)
{
    cout << "Sum of two members of class A and B is: ";
    cout << obj_a.data + obj_b.data;
    cout << "\n";
}

int main()
{
    A obj_a;
    B obj_b;

    add(obj_a, obj_b);

    return 0;
}
