/*
 * Write a program demonstrating multilevel inheritance
 */

#include <iostream>

using namespace std;

// base class
class Level1 {
	int var_level1;
	
	public:
	Level1()
	{
		cout << "Enter level1 integer value: ";
		cin >> var_level1;
	}
	int get_level1()
	{
		return var_level1;
	}
};

// child class of Level1
class Level2 : protected Level1 {
	int var_level2;
	
	public:
	Level2()
	{
		cout << "Enter level2 integer value: ";
		cin >> var_level2;
	}
	int get_level2()
	{
		return var_level2;
	}
};

// child class of Level2
class Level3 : private Level2 {
	int var_level3;
	
	public:
	// perform basic math operations
	void sum();
	void difference();
	void quotient();
	void product();
};

void Level3::sum()
{
	var_level3 = get_level1() + get_level2();
	cout << "Their sum is: ";
	cout << var_level3;
	cout << "\n";
}

void Level3::difference()
{
	var_level3 = get_level1() - get_level2();
	cout << "Their difference is: ";
	cout << var_level3;
	cout << "\n";
}

void Level3::quotient()
{
	var_level3 = get_level1() / get_level2();
	cout << "Their quotient is: ";
	cout << var_level3;
	cout << "\n";
}

void Level3::product()
{
	var_level3 = get_level1() * get_level2();
	cout << "Their product is: ";
	cout << var_level3;
	cout << "\n";
}

int main()
{
	Level3 object;
	
	object.sum();
	object.difference();
	object.quotient();
	object.product();
	
	return 0;
}
