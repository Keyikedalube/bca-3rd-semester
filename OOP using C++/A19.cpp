/*
 * Write a program demonstrating destructors
 */

#include <iostream>

using namespace std;

class Sentinel {
	public:
	Sentinel()
	{
		cout << "Constructor called\n";
	}
	~Sentinel()
	{
		cout << "Destructor called\n";
	}
};

int main()
{
	Sentinel object;
	
	return 0;
}
