#include <iostream>
#include <cstdlib>

int main()
{
    // seed
    // for the purpose of generating pseudo-random numbers
    srand(time(0));
    for (int i = 0; i < 10; i++)
        std::cout << rand() % 10 << " ";
    std::cout << "\n";

    return 0;
}
