/*
 * Write a program implementating a single inheritance of public data member.
 */

#include <iostream>

using namespace std;

// base class
class Base {
	public:
	int x;
	int y;
	Base();
	void get_x_y();
};

// derived class
class Derived : Base {
	int sum;
	
	public:
	void get_sum();
	void print_sum();
};

Base::Base()
{
	x = y = 0;
	get_x_y();
}

void Base::get_x_y()
{
	cout << "Enter x: ";
	cin >> x;
	cout << "Enter y: ";
	cin >> y;
}

void Derived::get_sum()
{
	sum = x + y;
}

void Derived::print_sum()
{
	cout << "Sum: ";
	cout << sum << "\n";
}

int main()
{
	Derived object;
	
	object.get_sum();
	object.print_sum();
	
	return 0;
}
