/*
 * Write a cpp program which explains the feature of an inline function.
 */

#include <iostream>

using namespace std;

inline int max(int, int);

int main()
{
	int a = 0;
	int b = 0;

	cout << "Enter integer a value: ";
	cin >> a;
	cout << "Enter integer b value: ";
	cin >> b;

	cout << "The max is: " << max(a, b);
	cout << endl;

	return 0;
}

inline int max(int a, int b)
{
	return a > b? a : b;
}

