/*
 * Write a cpp program for class with constructors.
 */

#include <iostream>

using namespace std;

class Sentinel {
    int x;

public:
    Sentinel();
    void display_x();
};

Sentinel::Sentinel()
{
    x = 32;
}

void Sentinel::display_x()
{
    cout << "The value of x is " << x << endl;
}

int main()
{
    Sentinel obj;

    obj.display_x();

    return 0;
}
