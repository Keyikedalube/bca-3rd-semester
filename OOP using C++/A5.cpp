/*
 * Write a program that will overload the function volume() three times.
 */

#include <iostream>
#include <cmath>

#define PI 3.14

using namespace std;

void volume(int radius)
{
	cout << "Volume of sphere is: ";
	cout << ((float)4/3) * PI * pow(radius, 3);
	cout << "\n";
}

void volume(int radius, int height)
{
	cout << "Volume of cylinder is: ";
	cout << PI * pow(radius, 2) * height;
	cout << "\n";
}

void volume(int length, int breadth, int height)
{
	cout << "Volume of rectangular prism is: ";
	cout << length * breadth * height;
	cout << "\n";
}

int main()
{
	unsigned short option;
	int tmp1 = 0;
	int tmp2 = 0;
	int tmp3 = 0;
	while (option != 4) {
		cout << "1\tSphere\n";
		cout << "2\tCylinder\n";
		cout << "3\tRectangular prism\n";
		cout << "4\tExit\n";
		cout << "\tEnter your option to compute volume: ";
		cin >> option;
		switch (option) {
			case 1:
				cout << "Enter radius value: ";
				cin >> tmp1;
				volume(tmp1);
				break;
			case 2:
				cout << "Enter radius of cylinder: ";
				cin >> tmp1;
				cout << "Enter height of cylinder: ";
				cin >> tmp2;
				volume(tmp1, tmp2);
				break;
			case 3:
				cout << "Enter length of rectangular prism: ";
				cin >> tmp1;
				cout << "Enter breadth of rectangular prism: ";
				cin >> tmp2;
				cout << "Enter height of rectangular prism: ";
				cin >> tmp3;
				volume(tmp1, tmp2, tmp3);
				break;
			case 4:
				cout << "Exiting program...\n";
				continue;
			default:
				cout << "Invalid option!";
				cout << endl;
		}
	}
	return 0;
}
