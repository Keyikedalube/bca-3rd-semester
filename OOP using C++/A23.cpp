/*
 * Write a program implementating single inheritance with private data member.
 */

#include <iostream>
#include <cstring>

#define SIZE 15

using namespace std;

// base class
class Parent {
	char character[SIZE];
	
	public:
	Parent();
	char* get_parent();
};

// derived class
class Child : Parent {
	char character[SIZE];
	
	public:
	void get_character();
	void print_child();
};

Parent::Parent()
{
	cout << "Enter Parent's character: ";
	cin >> character;
}

char* Parent::get_parent()
{
	return character;
}

void Child::get_character()
{
	// child inherits parent's character
	strcpy(character, get_parent());
}

void Child::print_child()
{
	cout << "Child's character: ";
	cout << character << "\n";
}

int main()
{
	Child Tony;
	
	Tony.get_character();
	Tony.print_child();
	
	return 0;
}

