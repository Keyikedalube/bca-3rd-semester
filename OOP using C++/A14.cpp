/*
 * Write a cpp program for swapping private data of two classes.
 */

#include <iostream>

using namespace std;

class Second;

class First {
	int datum;
	
	public:
	First()
	{
		datum = 1;
	}
	friend void swap(First, Second);
	friend void print(First, Second);
};

class Second {
	int datum;
	
	public:
	Second()
	{
		datum = 2;
	}
	friend void swap(First, Second);
	friend void print(First, Second);
};

void swap(First obj1, Second obj2)
{
	cout << "Before swapping:\n";
	print(obj1, obj2);
	int temp = obj1.datum;
	obj1.datum = obj2.datum;
	obj2.datum = temp;
	cout << "After swapping:\n";
	print(obj1, obj2);
}

void print(First obj1, Second obj2)
{
	cout << "obj1: " << obj1.datum;
	cout << "\n";
	cout << "obj2: " << obj2.datum;
	cout << "\n";
}

int main()
{
	First obj1;
	Second obj2;
	
	swap(obj1, obj2);
	
	return 0;
}
