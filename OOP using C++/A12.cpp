/*
 * Write a program demonstrating friend function
 */

#include <iostream>

using namespace std;

class Sentinel
{
    int data;

public:
    void set_data();
    friend void display_data(Sentinel);
};

void Sentinel::set_data()
{
    cout << "Enter any integer value: ";
    cin >> data;
}

void display_data(Sentinel obj)
{
    cout << "Data value is: ";
    cout << obj.data << "\n";
}

int main()
{
    Sentinel obj;

    obj.set_data();
    display_data(obj);

    return 0;
}
