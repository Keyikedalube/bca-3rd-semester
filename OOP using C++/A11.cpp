/*
 * Write a cpp program which explain concept of “object as arguments”
 */
 
#include <iostream>

using namespace std;

class Sentinel {
	int data;
	
	public:
	void get();
	void add(Sentinel, Sentinel);
	void print();
};

void Sentinel::get()
{
	cout << "Enter an integer value: ";
	cin >> data;
}

void Sentinel::add(Sentinel obj1, Sentinel obj2)
{
	data = obj1.data + obj2.data;
}

void Sentinel::print()
{
	cout << "The data value is: " << data;
	cout << "\n";
}

int main()
{
	Sentinel obj1, obj2, obj3;
	
	obj1.get();
	obj2.get();
	
	obj3.add(obj1, obj2);
	obj3.print();
	
	return 0;
}
