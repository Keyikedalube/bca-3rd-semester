/*
 * Write a program demonstrating use of constructor in derived class
 */

#include <iostream>

using namespace std;

class One {
	int x;
	
	public:
	One(int x) { this->x = x; }
	void display_x() { cout << "x = " << x << "\n"; }
};

class Two {
	float y;
	
	public:
	Two(float y) { this->y = y; }
	void display_y() { cout << "y = " << y << "\n"; }
};

class Three : public Two, public One {
	char c;
	
	public:
	Three(char c, float y, int x) : Two(y), One(x)	// constructor in derived class
	{
		this->c = c;
	}
	void display_c() { cout << "c = " << c << "\n"; }
};

int main()
{
	Three object('a', 5.5, 2);
	
	object.display_x();
	object.display_y();
	object.display_c();
	
	return 0;
}
