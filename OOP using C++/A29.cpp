/*
 * Write a program demonstrating initialization list in constructor
 */

#include <iostream>
#include <cstdlib>

using namespace std;

class VariableA {
	public:
		int x;
		
		VariableA(int arg)
		{
			x = arg;
		}
};

class VariableB: public VariableA {
	public:
		int y;
		
		VariableB(int arg): VariableA(rand() % 100)
		{
			y = arg;
		}
};

class VariableC: public VariableB {
	int sum;
	
	public:
	VariableC(): VariableB(rand() % 50)
	{
	}
	void sumOfVariableAB()
	{
		sum = x + y;
	}
	void print()
	{
		cout << "sum of ";
		cout << x << " and " << y;
		cout << " is " << sum << "\n";
	}
};

int main()
{
	VariableC obj;
	
	obj.sumOfVariableAB();
	obj.print();

	return 0;
}
