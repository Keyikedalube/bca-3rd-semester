/*
 * Write a cpp program which explains the use of scope resolution operator.
 */

#include <iostream>

int no = 10;

int main()
{
	int no = 0;
	std::cout << "Enter some number: ";
	std::cin >> no;

	std::cout << "Your number is: " << no << "\n";
	std::cout << "Computer's number is: " << ::no << "\n";

	return 0;
}
