/*
 * Write a program demonstrating virtual base class
 */
 
#include <iostream>

using namespace std;

class Base {
	int x;
	
	public:
	void set_x(int x)
	{
		this->x = x;
	}
	
	int get_x()
	{
		return x;
	}
};

class Derived1 : virtual public Base {
	int y;
	
	public:
	void set_y(int y)
	{
		this->y = y;
	}
	
	int get_y()
	{
		return y;
	}
};

class Derived2 : virtual public Base {
	int z;
	
	public:
	void set_z(int z)
	{
		this->z = z;
	}
	
	int get_z()
	{
		return z;
	}
};

class Derived3 : Derived1, Derived2 {
	int data;
	
	public:
	void set_values()
	{
		set_x(10);
		set_y(20);
		set_z(30);
	};
	
	void sum()
	{
		data = get_x() + get_y() + get_z();
	}
	
	void display()
	{
		cout << "Sum: " << data << "\n";
	}
};

using namespace std;

int main()
{
	Derived3 object;
	
	object.set_values();
	object.sum();
	object.display();
	
	return 0;
}
