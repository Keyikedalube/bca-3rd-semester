/*
 * Write a program demonstrating hybrid inheritance
 */

#include <iostream>

using namespace std;

class Employee {
	protected:
	int emp_code;
	
	public:
	void get_code(int);
	void put_code();
};

void Employee::get_code(int emp_code)
{
	this->emp_code = emp_code;
}

void Employee::put_code()
{
	cout << "Employee code: " << emp_code << "\n";
}

class Salary : public Employee {
	protected:
	float basic;
	float da;
	float hra;
	
	public:
	void get_sal(float, float, float);
	void put_sal();
};

void Salary::get_sal(float basic, float da, float hra)
{
	this->basic = basic;
	this->da = da;
	this->hra = hra;
}

void Salary::put_sal()
{
	cout << "Basic: " << basic << "\n";
	cout << "DA: " << da << "\n";
	cout << "HRA: " << hra << "\n";
}

class Deductions {
	protected:
	float ded;
	
	public:
	void get_ded(float);
	void put_ded();
};

void Deductions::get_ded(float ded)
{
	this->ded = ded;
}

void Deductions::put_ded()
{
	cout << "Deductions: " << ded << "\n";
}

class Payroll : public Salary, public Deductions {
	float gross;
	
	public:
	void display();
};

void Payroll::display()
{
	gross = basic + da + hra - ded;
	put_code();
	put_sal();
	put_ded();
	cout << "Gross: " << gross << "\n";
}

int main()
{
	Payroll emp1;
	
	emp1.get_code(101);
	emp1.get_sal(5000.0, 2400.0, 1800.0);
	emp1.get_ded(1200.0);
	emp1.display();
	
	return 0;
}

