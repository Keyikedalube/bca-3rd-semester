/*
 * Program demonstrating pass by reference
 */

#include <iostream>

using namespace std;

void print(int a, int b)
{
	cout << "a = " << a << endl;
	cout << "b = " << b << endl;
}


void swap(int &a, int &b)
{
	int temporary = a;
	a = b;
	b = temporary;
}

int main()
{
	int a, b;

	cout << "Enter the value of a and b: ";
	cin >> a >> b;

	swap(a, b);

	cout << "After swapping:" << endl;
	print(a, b);
	
	return(0);
}
