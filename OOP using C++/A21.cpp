/*
 * Write a program overloading binary plus (+) operator
 */

#include <iostream>

using namespace std;

class Binary_plus_overload {
	int datum;
	
	public:
	void get_datum();
	void print_datum();
	Binary_plus_overload operator +(Binary_plus_overload);
};

void Binary_plus_overload::get_datum()
{
	cout << "Enter any integer value: ";
	cin >> datum;
}

void Binary_plus_overload::print_datum()
{
	cout << datum << "\n";
}

Binary_plus_overload Binary_plus_overload::operator +(Binary_plus_overload object)
{
	Binary_plus_overload temp_object;
	temp_object.datum = this->datum + object.datum;
	return temp_object;
}

int main()
{
	Binary_plus_overload obj1, obj2, obj3;
	
	obj1.get_datum();
	obj2.get_datum();
	
	obj3 = obj1 + obj2;
	
	obj3.print_datum();
	
	return 0;
}
	
