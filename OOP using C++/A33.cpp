/*
 * Write a program to illustrate how pointers to a derived object are
 * used
 */

#include <iostream>

using namespace std;

class Base {
	int x;
	
	public:
	void display_x()
	{
		cout << "x = " << x << "\n";
	}
};

class Derived : public Base {
	int y;
	
	public:
	void display_y()
	{
		cout << "y = " << y << "\n";
	}
};

int main()
{
	Derived d1, *d2;
	
	d2 = &d1;
	
	d2->display_x();
	d2->display_y();
	
	return 0;
}
