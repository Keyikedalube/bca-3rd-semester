/*
 * Write a CPP program which shows use of "static member function"
 */

#include <iostream>

using namespace std;

class Sentinel {
    static int value;

public:
    static void set_value();
    static int get_value();
};

void Sentinel::set_value()
{
    value++;
}

int Sentinel::get_value()
{
    return value;
}

int Sentinel::value;

int main()
{
    Sentinel obj1, obj2;

    obj1.set_value();
    cout << "value's value is: " << obj1.get_value() << "\n";
    obj2.set_value();
    cout << "value's value is: " << obj2.get_value() << "\n";

    return 0;
}
