/*
 * Write a program which explain concept of a returning objects.
 */
 
#include <iostream>

using namespace std;
 
class Sentinel {
	int data;
	
	public:
	void get();
	friend Sentinel add(Sentinel, Sentinel);
	void print();
};

void Sentinel::get()
{
	cout << "Enter an integer value: ";
	cin >> data;
}

Sentinel add(Sentinel obj1, Sentinel obj2)
{
	Sentinel obj;
	obj.data = obj1.data + obj2.data;
	return obj;
}

void Sentinel::print()
{
	cout << "Data: " << data;
	cout << "\n";
}

int main()
{
	Sentinel obj1, obj2, obj3;
	
	obj1.get();
	obj2.get();
	obj3 = add(obj1, obj2);
	
	//obj1.print();
	//obj2.print();
	obj3.print();
	
	return 0;
}
