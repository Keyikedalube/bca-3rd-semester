/*
 * Write a program overloading unary minus (-) operator
 */

#include <iostream>

using namespace std;

class Unary_minus_overload {
	int datum;
	
	public:
	Unary_minus_overload()
	{
		datum = 10;
	}
	void operator -();
	void print();
};

void Unary_minus_overload::operator -()
{
	datum = -datum;
}

void Unary_minus_overload::print()
{
	cout << datum << "\n";
}

int main()
{
	Unary_minus_overload object;
	
	object.print();
	-object;
	object.print();
	
	return 0;
}
